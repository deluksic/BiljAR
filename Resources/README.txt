Everyone that is not working directly in the Unity editor should first put all resources like models, graphics etc. here, so that Unity devs can import them correctly.

Please be organized while uploading.

One more thing, usually what you will upload is a binary file. 
Single person - single file, please.
Do not create merges because you will lose data.

This file is also here, just to track this folder.